<?php
namespace app\admin\controller;
use app\home\model\News as NewsModel;
// use app\home\model\User as UserModel;
use app\home\model\NewsImage as Mod;
use app\home\validate\News as NewsValidate;
use think\facade\Request;
use yichenthink\utils\FileBase64;
use yichenthink\utils\ReturnMsg;

class NewsImage extends Base {
	// 查询列表
	public function list() {
		// $message = '没有数据';
		// $code = 400;
		// $map = [];
		// if (isset($category_id) && !empty($category_id)) {
		// 	$map[] = ['category_id', '=', $category_id];
		// }
		// // 查询未删除的
		// $data = Mod::with(['newsContent' => function ($query) {
		// 	$query->field(['content', 'image', 'title', 't_id']);
		// }])->where($map)->select();
		// if ($data) {
		// 	$count = count($data);
		// 	for ($i = 0; $i < $count; $i++) {
		// 		$data[$i]->abstract = mb_substr($data[$i]->content, 0, 100);
		// 	}
		// 	$code = 200;
		// 	$message = '成功';
		// }
		// ReturnMsg::returnMsg($code, $message, $data->visible(['id', 'title', 'create_time', 'update_time', 'abstract', 'image']));
	}
	public function detailedList($t_id) {
		$Mod = Mod::where('t_id', $t_id)->select();
		$message = '';
		$code = 400;
		if ($Mod) {
			$code = 200;
		}
		ReturnMsg::returnMsg($code, $message, $Mod);
	}
	public function upload($t_id, $image) {
		$code = 400;
		$message = '';
		$map = [];
		$map[] = ['user_id', '=', $this->userInfo['uid']];
		$map[] = ['id', "=", $t_id];

		$NewsModel = NewsModel::where($map)->find();
		$newsImage = [];
		if ($NewsModel) {
			$FileBase64 = new FileBase64;
			$imageData = [];
			//获取base64流的信息
			if ($FileBase64->getBase64Info($image)) {
				// 预备上传 返回图片上传后的信息 路径名 图片类型，尺寸等信息
				$FileBase64->readyUpload('news/' . date('y/m/d/'));
				//获取图片地址
				// 附件图片表将要存入详细信息
				$imageData = [
					't_id' => $t_id,
					'suffix' => $FileBase64->suffix,
					'src' => $FileBase64->url,
					'user_id' => $this->userInfo['uid'],
					'size' => $FileBase64->size,
					'content_type' => $FileBase64->contentType,
					'type' => $FileBase64->type,
				];
				$newsImage = new Mod;
				// 详细数据保存到图片表
				if ($newsImage->save($imageData)) {
					$FileBase64->upload();
					$code = 200;
				} else {
					$message = "上传失败";
				}

			};

		}
		ReturnMsg::returnMsg($code, $message, $newsImage);

	}
	// 删除一条
	public function delete($id = 0, $isTrue = true, $tid = null, $src = null) {

		$map = [['user_id', '=', $this->userInfo['uid']]];
		if (!empty($src) && !empty($tid)) {
			$map[] = ['t_id', '=', $tid];
			$map[] = ['src', '=', $src];
		} else {
			$map[] = ['id', '=', $id];
		}
		$code = 400;
		$Mod = Mod::withTrashed()->where($map)
			->find();
		ReturnMsg::returnMsg($code, '', $Mod);
		if ($Mod && isset($Mod->src)) {
			// 取出图片
			$FileBase64 = new FileBase64;
			// 删除图片资源
			$FileBase64->destroy($Mod->src);

			if ($isTrue) {
				$Mod->delete($isTrue);
				# code...
			} else {
				$Mod->delete($isTrue);
			}
			$code = 200;

		}

		ReturnMsg::returnMsg($code, '', $Mod);
	}

	//恢复一条
	public function restore($id = 0) {

		$map = [];
		$map[] = ['id', '=', $id];
		$code = 400;
		$Mod = Mod::onlyTrashed()
			->where($map)
			->find();
		# code...
		if ($Mod) {
			$code = 200;
			$Mod->restore();
		}
		// $user = UserModel::restore($list);
		ReturnMsg::returnMsg($code);
	}

}
