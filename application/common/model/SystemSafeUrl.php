<?php
namespace app\common\model;
use think\model\concern\SoftDelete;

class SystemSafeUrl extends Model {
	use SoftDelete;
	protected $deleteTime = 'delete_time'; //软删除字段
	protected $defaultSoftDelete = 0;

}