<?php
namespace app\home\model;

class News extends \app\common\model\News {
	// 关联内容模型
	public function newsContent() {
		return $this->hasOne('NewsContent', 't_id', 'id')->bind([
			'title' => 'title',
			'content' => 'content',
			'image' => 'image',
			't_id' => 't_id',
		]);
	}
	// 关联图片模型
	public function newsImage() {
		return $this->hasMany('newsImage', 't_id', 'id');
	}
}